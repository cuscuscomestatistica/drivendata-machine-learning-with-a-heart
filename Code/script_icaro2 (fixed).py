# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 10:06:33 2018

@author: icaromarley5
"""

'''
Esse script abandona a análise dos dados para obter uma maior precisão na estimativa do erro.
Se quiser saber mais porque isso acontece, me pergunte

Aqui eu usei 2 ferramentas avançadas do Sklearn, Pipeline e GridSearch
Pipeline serve para transformar todo o processo de preprocessamento e construção do modelo em um modelo
Por exemplo, em vez de criar um scaler e um modelo, eu crio uma pipe com um scaler e um modelo e trato ambos como um objeto só

GridSearch serve para buscar a configuração ótima dos parametros de cada modelo (também chamados de hiperparâmetros). Você especifica o que quer buscar.
GridSearch usa validação cruzada (k-fold) para descobrir os melhores parâmetros
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import MinMaxScaler,Imputer,StandardScaler,MaxAbsScaler,RobustScaler
from sklearn.metrics import classification_report,log_loss,make_scorer
from sklearn.feature_selection import SelectFromModel
from sklearn.model_selection import ParameterGrid,GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.base import clone
from sklearn.base import BaseEstimator, TransformerMixin
# data description 
# https://www.drivendata.org/competitions/54/machine-learning-with-a-heart/page/109/

# load data
data_path = '../Modified Data/'
df_test = pd.read_csv(data_path+'Warm_Up_Machine_Learning_with_a_Heart_-_Test_Values.csv')
df_train_x = pd.read_csv(data_path+'Warm_Up_Machine_Learning_with_a_Heart_-_Train_Values.csv')
df_train_y = pd.read_csv(data_path+'Warm_Up_Machine_Learning_with_a_Heart_-_Train_Labels.csv')
df_train = df_train_x.merge(df_train_y,on='patient_id')
del df_train_x,df_train_y

# preprocessing
# code thal
column = 'thal'
to_replace = {
    'normal':0,
    'fixed_defect':1,
    'reversible_defect':2
}
df_train[column] = df_train[column].replace(to_replace)
df_test[column] = df_test[column].replace(to_replace)

target = 'heart_disease_present' # variável alvo
# lista de variáveis selecionadas para modelar a variável alvo
columns = ['slope_of_peak_exercise_st_segment', 'thal',
       'resting_blood_pressure', 'chest_pain_type', 'num_major_vessels',
       'fasting_blood_sugar_gt_120_mg_per_dl', 'resting_ekg_results',
       'serum_cholesterol_mg_per_dl', 'oldpeak_eq_st_depression', 'sex', 'age',
       'max_heart_rate_achieved', 'exercise_induced_angina']
X_train = df_train[columns].values
y_train = df_train[target].values
X_test = df_test[columns].values

# pipeline    
model = Pipeline([
        ("imputer", Imputer(missing_values=np.nan,
            strategy='most_frequent',
            axis=0)),
        ('scaler',StandardScaler()),
        ('feature_selection',
            SelectFromModel(LogisticRegression(random_state=100,max_iter=10000,))),
        ('model',LogisticRegression(random_state=100,max_iter=10000))
])
param_grid = [{
    'scaler':[StandardScaler(),MinMaxScaler(),MaxAbsScaler(),RobustScaler()],
    'feature_selection__estimator__C':np.arange(0.1,1,0.05),
    'feature_selection__estimator__penalty': ['l1', 'l2'],
}]
grid = GridSearchCV(model, cv=10, n_jobs=1, param_grid=param_grid,
    scoring=lambda model,x,y:-log_loss(y,model.predict_proba(x)),verbose=1)
grid.fit(X_train,y_train)

mean_scores = np.array(grid.cv_results_['mean_test_score'])
std_scores = np.array(grid.cv_results_['std_test_score'])
df_results = pd.DataFrame([mean_scores,std_scores]).T
df_results.columns = ['mean','std']
params = [str(params) for params in grid.cv_results_['params']]
df_results['params'] = params

model = grid.best_estimator_

# metrics
print(classification_report(model.predict(X_train),y_train))
print('Log Loss',log_loss(y_train,model.predict_proba(X_train)))
# resultados bons para o treinamento, porem o teste pode nao ter o mesmo desempenho
# predictions on test set
predictions = pd.Series(model.predict_proba(X_test)[:, 1],name=target)
# save predictions
result_path = '../Results/'
df_predictions = pd.DataFrame()
df_predictions['patient_id'] = df_test['patient_id']
df_predictions[target] = predictions.astype(float)
df_predictions.to_csv(result_path+'predictions.csv',sep=',',index=False)
# submeter arquivo no site