import pandas as pd # manipulação de tabela
import numpy as np # manipulação de vetores
import matplotlib.pyplot as plt  # gráficos
import seaborn as sns # gráficos
from sklearn.linear_model import LogisticRegression # classificador linear
from sklearn.metrics import classification_report,log_loss # métricas de avaliação

'''
Sugiro que foque nesse script primeiro.
De uma olhada na parte de análise e no preprocessamento que eu fiz
Não tem nada muito complexo.

Eu não cheguei a entender o domínio (doença do coração) enquanto fazia esses scripts.
A parte mais importante na modelagem é o preprocessamento
É onde a gnt limpa os dados e cria variáveis novas
Essas variáveis novas são extremamente importantes para o mapeamento do problema
E são decisivas para subir no ranking da competição
E para criar essas variáveis, é necessário entender o problema.

Por exemplo: 
    Num sistema educacional, eu quero prever a nota do aluno no semestre.
    Eu tenho as disciplinas que ele pega no semestre e o histórico dele.
    Conhecendo o domínio e sabendo que alunos com uma grande quantidade de disciplinas vai passar dificuldades, eu posso criar a variável 'quantidade de disciplinas' a partir da soma de todas as disciplinas que o aluno pegou no semestre.

Enfim, vai ficar como dever de casa nosso entender bem o domínio (doenças do coração)
Para poder criar novas variáveis que sejam relevantes para mapear o problema

PS:
    Fique a vontade para criar o seu próprio script a partir desse.
    Eu tentei deixar esse script legível. Não consegui muito. UAEHUAEHUAE
    Mexa um pouco e teste a submissão no site. Detalhe, o modelo que vc criar provavelmente n vai ficar bom, pq eu suprimi algumas operações necessárias para melhorar o desempenho dele. (quis simplificar o código)
    Mas, no geral, o código está funcional

O modelo de classificação lienar que estou utilizando é o Logistic Regression, de uma olhada no youtube para aprender mais a respeito
É importante saber como ele funciona e em que tipos de situação ele é melhor utilizado.
Eu discuto um pouco sobre isso mais embaixo
'''
'''
carregar dados
tem uma descrição sobre cada coluna aqui
https://www.drivendata.org/competitions/54/machine-learning-with-a-heart/page/109/
'''
data_path = '../Modified Data/'
df_test = pd.read_csv(data_path+'Warm_Up_Machine_Learning_with_a_Heart_-_Test_Values.csv')
df_train_x = pd.read_csv(data_path+'Warm_Up_Machine_Learning_with_a_Heart_-_Train_Values.csv')
df_train_y = pd.read_csv(data_path+'Warm_Up_Machine_Learning_with_a_Heart_-_Train_Labels.csv')
df_train = df_train_x.merge(df_train_y,on='patient_id')
del df_train_x,df_train_y

# análise
'''
é um problema de classificação, logo, nessa parte é necessário
>encontrar variáveis imporantes para a classificação
>detectar variáveis ruins e problemáticas
>manipular dados 
>criar variáveis novas
>selecionar variáveis
>etc
'''
df_train.describe() # percentils
df_train.isnull().any() # não possui valores nulos

target = 'heart_disease_present' # variável alvo

column = 'slope_of_peak_exercise_st_segment'
print(df_train[column].value_counts())
sns.barplot(x=column,y=target,data=df_train)
plt.show()
'''
paciente do tipo 1 tem menos chances que os 2 e 3
nao sei exatamente o que significa esses numeros, é interessante pesquisar a respeito
12 do tipo 3
talvez seja melhor unir 2 e 3 em uma so categoria
'''
column = 'thal'
print(df_train[column].value_counts())
sns.barplot(x=column,y=target,data=df_train)
plt.show()
'''
normal tem menos chance
reversible tem mais
nao se pode afirmar algo sobre fixed
variavel pode ser codificada para int -> normal 0, 1 fixed, reversible 2
8 fixed
'''
sns.violinplot(y='resting_blood_pressure',x=target,data=df_train)
'''
nao parece afetar a variavel alvo
'''
column = 'chest_pain_type'
print(df_train[column].value_counts())
sns.barplot(x=column,y=target,data=df_train)
plt.show()
'''
paciente do tipo 4 possui mais chance
2 possui menos
3 possui menos, porem amsi que 2
1 possui menos que 4 e mais que 2 e 3
13 do tipo 1
'''
column = 'num_major_vessels'
print(df_train[column].value_counts())
sns.barplot(x=column,y=target,data=df_train)
plt.show()
'''
0 < 1 < 2 < 3
14 do tipo 3
maior parte do tipo 0
'''
column = 'fasting_blood_sugar_gt_120_mg_per_dl'
print(df_train[column].value_counts())
sns.barplot(x=column,y=target,data=df_train)
plt.show()
'''
nao parece ter tanto efeito
so 29 pacientes do tipo 1
desconsiderar variavel?
'''
column = 'resting_ekg_results'
print(df_train[column].value_counts())
sns.barplot(x=column,y=target,data=df_train)
plt.show()
'''
0 < 2 < 1
só existe 1 paciente do tipo 1, desconsiderar valor
codar em 0,1
'''
sns.violinplot(y='serum_cholesterol_mg_per_dl',x=target,data=df_train)
plt.show()
'''
similares
pacientes sem doença possuem um amplo range de valores nessa variavel
'''
sns.violinplot(y='oldpeak_eq_st_depression',x=target,data=df_train)
plt.show()
'''
pacientes sem doenca possuem um menor valor nessa variavel
'''
column = 'sex'
print(df_train[column].value_counts())
sns.barplot(x=column,y=target,data=df_train)
plt.show()
'''
56 do tipo 0
tipo zero possui menos chance
'''
sns.violinplot(y='age',x=target,data=df_train)
plt.show()
'''
pacientes com doença possuem um amplo range nessa variavel
'''
sns.violinplot(y='max_heart_rate_achieved',x=target,data=df_train)
plt.show()
'''
uma grande quantidade de pacientes com maiores valores nessa variavel nao possuem doenca
'''
column = 'exercise_induced_angina'
print(df_train[column].value_counts())
sns.barplot(x=column,y=target,data=df_train)
plt.show()
'''
tipo 0 possuem menos chance
'''

# preprocessing
'''
codifiquei algumas variáveis.
Todas essas operações que fiz abaixo foram para consertar problemas identificados na fase de análise.
Como a base de dados veio de uma competição, ela está bem padronizada e limpa
Então não há muito o que fazer em relação a 'consertar'

principalmente a variável categórica, 'thal'
os algoritmos do sklearn não lidam com variáveis categoricas, só numéricas
por sorte, thal indica uma ordem, então pode ser codificada diretamente para valores númericos, sem perda de informação
'''
# code slope_of_peak_exercise_st_segment -> 0,1 values
column = 'slope_of_peak_exercise_st_segment'
to_replace = {1:0,2:1,3:1}
df_train[column] = df_train[column].replace(to_replace)
df_test[column] = df_test[column].replace(to_replace)
# code thal
column = 'thal'
to_replace = {
    'normal':0,
    'fixed_defect':1,
    'reversible_defect':2
}
df_train[column] = df_train[column].replace(to_replace)
df_test[column] = df_test[column].replace(to_replace)
# code resting_ekg_results -> 0,1
to_replace = {2:1}
column = 'resting_ekg_results'
df_train[column] = df_train[column].replace(to_replace)
df_test[column] = df_test[column].replace(to_replace)

# lista de variáveis selecionadas para modelar a variável alvo
columns = ['slope_of_peak_exercise_st_segment','thal',
           'chest_pain_type','num_major_vessels','resting_ekg_results',
           'serum_cholesterol_mg_per_dl','oldpeak_eq_st_depression','sex',
           'age','max_heart_rate_achieved','exercise_induced_angina',] 

'''
os modelos do sklearn são treinados com a função fit
fit aceita X,y 
X são as variáveis que explicam y
e a função do modelo é tentar entender como

Obviamente, se vc escolher Xs que não explicam y direito, o modelo n vai fazer um bom trabalho no mapeamento

Ditado no data science: "lixo entra, lixo sai". se vc não tiver dados bons, não terá bons resultados
'''
X_train = df_train[columns].values
y_train = df_train[target].values
X_test = df_test[columns].values
'''
Você vai entender melhor minha escolha quando estudar sobre esse modelo
Eu escolhi ele porque ele é menos complexo que outros modelos e por isso tem menos chance de 
overfitar (sugiro que leia a respeito, overfitting em machine learning) com poucos dados
temos basicamente 180 dados de treinamento, o que é muito pouco.
'''
model = LogisticRegression(random_state=100,max_iter=10000) # classificador linear e seus hiperparâmetros
# training
model.fit(X_train,y_train)
# metrics
print(classification_report(model.predict(X_train),y_train))
print('Log Loss',log_loss(y_train,model.predict_proba(X_train)))
'''
sugiro que leia sobre cada uma dessas métricas
resultados bons para o treinamento, porem o teste pode nao ter o mesmo desempenho
a métrica utilizada no site é o Log Loss, por isso uso ela aqui.
'''
# prevendo valores no test set
predictions = pd.Series(model.predict_proba(X_test)[:, 1],name=target)
'''
os modelos do sklearn, pra classificação, possuem duas funções, predict e predict_proba
predict retorna a classe a ser prevista, 0,1,2, etc
predict proba retorna uma lista com a probabilidade do dado pertencer a cada classe, somando em 1, exemplo [.2,.8]. Nesse caso, o dado tem mais chance de pertencer a classe 2
O problema da submissão, que fez eu perder pontuação foi que a função que eles queriam que eu usasse era a predict_proba, e eu estava usando a predict
ai, o predict "arredonda" as probabilidades, o que fez a Log Loss aumentar bastante
'''
# save predictions
'''
o arquivo de saida está na pasta Results.
É só submeter ele no site
'''
result_path = '../Results/'
df_predictions = pd.DataFrame()
df_predictions['patient_id'] = df_test['patient_id']
df_predictions[target] = predictions.astype(float)
df_predictions.to_csv(result_path+'predictions.csv',sep=',',index=False)
# submeter arquivo no site