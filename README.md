# DrivenData: Machine Learning with a Heart

Repositório para submissões relacionadas à competição do DrivenData "Machine Learning with a Heart", cujo foco é entender as causas de doenças cardíacas.
URL da competição: https://www.drivendata.org/competitions/54/machine-learning-with-a-heart/

A melhor posição obtida no ranking da competição foi o 5º lugar, em 24/07/2018

# Tecnologias utilizadas
- Pandas e Numpy (Manipulação de dados)
- Matplotlib e Seaborn (Visualização de dados)
- Sklearn (Machine Learning)